using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Pages
{
    [ValidateAntiForgeryToken]
    public class ArticleModel : PageModel
    {
        private readonly ArticleStorage _dataStore;
        private readonly HtmlEncoder _htmlEncoder;
        private const int MaxAllowedComments = 100;

        public ArticleModel(ArticleStorage dataStore, HtmlEncoder htmlEncoder)
        {
            _dataStore = dataStore;
            _htmlEncoder = htmlEncoder;
        }

        
        public Article Article { get; set; }

        
        public bool IsCommentsFull => Article.Comments.Count >= MaxAllowedComments;

        
        public HtmlString HtmlBody()
        {
            HtmlString html = MarkdownRenderer.RenderMarkdown(Article.Body);
            return html;
        }

        
        public void OnGet([FromRoute] string id)
        {
            Article = _dataStore.GetArticle(id);

            if (Article == null || !Article.IsPublic)
            {
                RedirectToPage("/Index");
            }
        }


        private IActionResult ChangeState(bool deleted, string id)
        {
            Article = _dataStore.GetArticle(id);

            if (!ModelState.IsValid)
                return LocalRedirect("/article/" + id + "/" + Article.Slug);

            Article.IsDeleted = deleted;
            _dataStore.SaveArticle(Article);
            return RedirectToPage("/Index");
        }


        public IActionResult OnPostDeleteArticle([FromRoute] string id)
        {
            string safeId = _htmlEncoder.Encode(id);
            return ChangeState(true, safeId);
        }

        

        public IActionResult OnPostUnDeleteArticle([FromRoute] string id)
        {
            string safeId = _htmlEncoder.Encode(id);
            return ChangeState(false, safeId);
        }


        public IActionResult OnPostPublishComment([FromRoute] string id,
            [FromForm] CommentViewModel newComment)
        {
            string safeId = _htmlEncoder.Encode(id);
            Article = _dataStore.GetArticle(safeId);

            if (Article == null)
            {
                return RedirectToPage("/Index");
            }

            if (!ModelState.IsValid) return Page();
            var comment = new Comment
            {
                Author = newComment.Author,
                Body = newComment.Body,
                IsPublic = true,
                UniqueId = Guid.NewGuid()
            };
            Article.Comments.Add(comment);
            _dataStore.SaveArticle(Article);

            return LocalRedirect("/article/" + safeId + "/" + Article.Slug);
        }
        
        
        public IActionResult OnPostDeleteComment(Guid commentId, string id)
        {
            string safeId = _htmlEncoder.Encode(id);
            Article article = _dataStore.GetArticle(safeId);

            Comment foundComment = ArticleStorage.FindComment(commentId, article);
            foundComment.IsPublic = false;

            _dataStore.SaveArticle(article);
            
            return LocalRedirect("/article/" + safeId + "/" + article.Slug);
        }


        public IActionResult OnPostUnDeleteComment(Guid commentId, string id)
        {           
            string safeId = _htmlEncoder.Encode(id);
            Article article = _dataStore.GetArticle(safeId);

            Comment foundComment = ArticleStorage.FindComment(commentId, article);
            foundComment.IsPublic = true;

            _dataStore.SaveArticle(article);

            return LocalRedirect("/article/" + safeId + "/" + article.Slug);
        }


        public class CommentViewModel
        {
            [Required]
            [MaxLength(100, ErrorMessage = "You have exceeded the maximum " +
                                           "length of 100 characters")]
            public string Author { get; set; }

            [Required]
            [MaxLength(1000, ErrorMessage = "You have exceeded the maximum " +
                                            "length of 1000 characters")]
            public string Body { get; set; }
        }
    }
}