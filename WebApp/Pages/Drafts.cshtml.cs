using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;

namespace WebApp.Pages
{
    [Authorize(Policy = "CreateArticle")]
    public class DraftsModel : PageModel
    {
        private readonly ArticleStorage _dataStore;

        public IEnumerable<DraftSummaryModel> DraftSummaries { get; private set; }

        public DraftsModel(ArticleStorage dataStore)
        {
            _dataStore = dataStore;
        }

        public void OnGet()
        {
            bool ArticleFilter(Article p) => !p.IsPublic;
            IEnumerable<Article> articleModels =
                _dataStore.GetAllDrafts().Where(ArticleFilter);

            DraftSummaries = articleModels.Select(p => new DraftSummaryModel
            {
                Author = p.Author,
                Id = p.Id,
                Slug = p.Slug,
                Title = p.Title,
                Excerpt = p.Excerpt,
                PublishTime = p.PubDate,
                CommentCount = p.Comments.Count
            });
        }
    }

    
    public class DraftSummaryModel
    {
        public string Author { get; set; }
        public Guid Id { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public DateTimeOffset PublishTime { get; set; }
        public string Excerpt { get; set; }
        public int CommentCount { get; set; }
    }
}