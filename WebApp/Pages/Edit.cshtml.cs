using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Pages
{
    [ValidateAntiForgeryToken]
    [Authorize(Policy = "EditArticle")]
    public class EditModel : PageModel
    {
        private readonly ArticleStorage _dataStore;
        private readonly ExcerptGenerator _excerptGenerator;
        private readonly HtmlEncoder _htmlEncoder;

        public EditModel(ArticleStorage dataStore,
            ExcerptGenerator excerptGenerator, HtmlEncoder htmlEncoder)
        {
            _dataStore = dataStore;
            _excerptGenerator = excerptGenerator;
            _htmlEncoder = htmlEncoder;
        }

        [BindProperty] public EditedArticleModel EditedArticle { get; set; }

        public bool HasSlug { get; set; }


        public void OnGet([FromRoute] string id)
        {
            Article article = _dataStore.GetArticle(id);

            if (article == null)
            {
                RedirectToPage("/Index");
            }
            else
            {
                EditedArticle = new EditedArticleModel
                {
                    Title = article.Title,
                    Body = article.Body,
                    Excerpt = article.Excerpt
                };

                HasSlug = !string.IsNullOrEmpty(article.Slug);

                string iDNoHyphens = article.Id.ToString()
                    .Replace("-", "");
                ViewData["Slug"] = article.Slug;
                ViewData["id"] = iDNoHyphens;
            }
        }


        public IActionResult OnPostPublish([FromRoute] string id,
            [FromForm] bool updateSlug)
        {
            string safeId = _htmlEncoder.Encode(id);
            Article article = _dataStore.GetArticle(safeId);
            if (!ModelState.IsValid)
            {
                return LocalRedirect("/article/" + safeId + "/" + article.Slug);
            }

            bool wasPublic = article.IsPublic;
            article.IsPublic = true;
            if (article.PubDate.Equals(default(DateTimeOffset)))
            {
                article.PubDate = DateTimeOffset.Now;
            }

            if (!HasSlug)
            {
                article.Slug = SlugGenerator.CreateSlug(article.Title);
            }

            UpdateArticle(article, updateSlug, wasPublic);

            return LocalRedirect("/article/" + safeId + "/" + article.Slug);
        }


        public IActionResult OnPostSaveDraft([FromRoute] string id)
        {
            string safeId = _htmlEncoder.Encode(id);
            Article article = _dataStore.GetArticle(safeId);
            if (!ModelState.IsValid) return LocalRedirect("/Drafts");

            bool wasPublic = article.IsPublic;
            article.IsPublic = false;
            UpdateArticle(article, false, wasPublic);

            return LocalRedirect("/Drafts");
        }


        private void UpdateArticle(Article article, [FromForm] bool updateSlug,
            bool wasPublic)
        {
            article.Title = EditedArticle.Title;
            article.Body = EditedArticle.Body;
            if (string.IsNullOrEmpty(EditedArticle.Excerpt))
            {
                EditedArticle.Excerpt =
                    _excerptGenerator.CreateExcerpt(EditedArticle.Body);
            }

            article.Excerpt = EditedArticle.Excerpt;

            if (updateSlug)
            {
                article.Slug = SlugGenerator.CreateSlug(article.Title);
            }

            if (Request != null)
            {
                _dataStore.SaveFiles(Request.Form.Files.ToList());
            }

            _dataStore.UpdateArticle(article, wasPublic);
        }


        public class EditedArticleModel
        {
            [Required] public string Title { get; set; }
            [Required] public string Body { get; set; }
            public string Excerpt { get; set; }
        }
    }
}