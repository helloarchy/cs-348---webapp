using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;

namespace WebApp.Pages
{
    [Authorize(Policy = "DeleteArticle")]
    public class RecycleBinModel : PageModel
    {
        private readonly ArticleStorage _dataStore;

        public IEnumerable<ArticleSummaryModel> ArticleSummaries { get; private set; }

        public RecycleBinModel(ArticleStorage dataStore)
        {
            _dataStore = dataStore;
        }

        public void OnGet()
        {
            bool DeletedArticleFilter(Article p) => p.IsDeleted;
            IEnumerable<Article> articleModels = 
                _dataStore.GetAllArticles().Where(DeletedArticleFilter);

            ArticleSummaries = articleModels.Select(p => new ArticleSummaryModel
            {
                Id = p.Id,
                Slug = p.Slug,
                Title = p.Title,
                Excerpt = p.Excerpt,
                PublishTime = p.PubDate,
                CommentCount = p.Comments.Count(c => c.IsPublic)
            });
        }

        public class ArticleSummaryModel
        {
            public Guid Id { get; set; }
            public string Slug { get; set; }
            public string Title { get; set; }
            public DateTimeOffset PublishTime { get; set; }
            public string Excerpt { get; set; }
            public int CommentCount { get; set; }
        }
    }
}
