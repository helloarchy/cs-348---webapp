using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ArticleStorage _dataStore;

        public IEnumerable<ArticleSummaryModel> ArticleSummary { get; private set; }

        public IndexModel(ArticleStorage dataStore)
        {
            _dataStore = dataStore;
        }

        public void OnGet()
        {
            bool ArticleFilter(Article p) => p.IsPublic;
            bool DeletedArticleFilter(Article p) => !p.IsDeleted;
            IEnumerable<Article> postModels = _dataStore.GetAllArticles()
                .Where(ArticleFilter)
                .Where(DeletedArticleFilter);

            ArticleSummary = postModels.Select(p => new ArticleSummaryModel
            {
                Id = p.Id,
                Slug = p.Slug,
                Title = p.Title,
                Excerpt = p.Excerpt,
                PublishTime = p.PubDate,
                CommentCount = p.Comments.Count(c => c.IsPublic)
            });
        }

        public class ArticleSummaryModel
        {
            public Guid Id { get; set; }
            public string Slug { get; set; }
            public string Title { get; set; }
            public DateTimeOffset PublishTime { get; set; }
            public string Excerpt { get; set; }
            public int CommentCount { get; set; }
        }
    }
}