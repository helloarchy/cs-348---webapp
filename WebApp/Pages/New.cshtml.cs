using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Pages
{
    [ValidateAntiForgeryToken]
    [Authorize(Policy = "CreateArticle")]
    public class NewModel : PageModel
    {
        private readonly ArticleStorage _dataStore;
        private readonly ExcerptGenerator _excerptGenerator;

        public NewModel(ArticleStorage dataStore,
            ExcerptGenerator excerptGenerator)
        {
            _dataStore = dataStore;
            _excerptGenerator = excerptGenerator;
        }

        
        [BindProperty] 
        public NewArticleViewModel NewArticle { get; set; }

        
        public void OnGet()
        {
        }

        
        public IActionResult OnPostPublish()
        {
            if (!ModelState.IsValid) return Page();
            SaveArticle(NewArticle, true);
            return LocalRedirect("/Index");
        }

        
        public IActionResult OnPostSaveDraft()
        {
            if (!ModelState.IsValid) return Page();
            SaveArticle(NewArticle, false);
            return LocalRedirect("/Drafts");
        }

        
        private void SaveArticle(NewArticleViewModel newArticle,
            bool publishArticle)
        {
            if (string.IsNullOrEmpty(newArticle.Excerpt))
            {
                newArticle.Excerpt = _excerptGenerator
                    .CreateExcerpt(newArticle.Body);
            }

            if (string.IsNullOrEmpty(newArticle.Author))
            {
                newArticle.Author = "Anonymous";
            }

            var article = new Article
            {
                Author = newArticle.Author,
                Title = newArticle.Title,
                Body = newArticle.Body,
                Excerpt = newArticle.Excerpt,
                Slug = SlugGenerator.CreateSlug(newArticle.Title),
                LastModified = DateTimeOffset.Now,
                IsDeleted = false
            };

            if (publishArticle)
            {
                article.PubDate = DateTimeOffset.Now;
                article.IsPublic = true;
            }

            if (Request != null)
            {
                _dataStore.SaveFiles(Request.Form.Files.ToList());
            }

            _dataStore.SaveArticle(article);
        }

        
        public class NewArticleViewModel
        {
            [Required] public string Author { get; set; }
            [Required] public string Title { get; set; }
            [Required] public string Body { get; set; }
            public string Excerpt { get; set; }
        }
    }
}