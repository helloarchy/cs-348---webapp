﻿using System.Text.RegularExpressions;

namespace WebApp.Services
{
    public class SlugGenerator
    {
        public static string CreateSlug(string title)
        {
            string tempTitle = title;
            tempTitle = tempTitle.Replace(" ", "-");
            var allowList = new Regex("([^A-Za-z0-9-])");
            string slug = allowList.Replace(tempTitle, "");
            return slug;
        }
    }
}
