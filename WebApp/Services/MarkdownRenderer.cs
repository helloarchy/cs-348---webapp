﻿using Markdig;
using Microsoft.AspNetCore.Html;

namespace WebApp.Services
{
    public class MarkdownRenderer
    {
        private static readonly MarkdownPipeline Pipeline = 
            new MarkdownPipelineBuilder()
            .UseDiagrams()
            .UseAdvancedExtensions()
            .UseYamlFrontMatter()
            .DisableHtml()
            .Build();

        public static HtmlString RenderMarkdown(string bodyText)
        {
            string html = Markdown.ToHtml(bodyText, Pipeline);
            return new HtmlString(html);
        }
    }
}
