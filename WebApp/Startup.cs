﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.AddMvc()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AuthorizeFolder("/Account/Manage");
                    options.Conventions.AuthorizePage("/Account/Logout");
                });
            
            services.AddAuthorization(options =>
            {
                // Role based claims
                options.AddPolicy("MemberOnly",
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("CustomerOnly",
                    policy => policy.RequireClaim("Customer"));
                
                //  Specific claims
                options.AddPolicy("ManageRoles", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("ManageUsers", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("AddComment", 
                    policy => policy.RequireClaim("Customer"));
                options.AddPolicy("DeleteAnyComment", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("DeleteOwnComment", 
                    policy => policy.RequireClaim("Customer"));
                options.AddPolicy("CreateComment", 
                    policy => policy.RequireClaim("Customer"));
                options.AddPolicy("CreateArticle", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("DeleteArticle", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("EditArticle", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("EditAnyComment", 
                    policy => policy.RequireClaim("Member"));
                options.AddPolicy("EditOwnComment", 
                    policy => policy.RequireClaim("Customer"));
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IFileSystem, PhysicalFileSystem>();
            services.AddSingleton<ArticleStorage>();
            services.AddSingleton<SlugGenerator>();
            var excerptGenerator = new ExcerptGenerator(140);
            services.AddSingleton(excerptGenerator);
            services.AddSingleton<MarkdownRenderer>();
        }

        // This method gets called by the runtime. Use this method to configure
        // the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, 
            ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();

            // Seed database
            DbSeed.SeedDatabase(context, userManager).Wait();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}