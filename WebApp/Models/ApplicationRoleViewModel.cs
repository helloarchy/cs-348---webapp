﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class ApplicationRoleViewModel
    {
        public ApplicationRole ApplicationRole { get; set; }
        public IList<ApplicationUser> ApplicationUsers { get; set; }

        // For adding a user to the role
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} " +
                                         "characters long.", MinimumLength = 2)]
        public string Role { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} " +
                                         "characters long.", MinimumLength = 2)]
        public string UserName { get; set; }
    }
}
