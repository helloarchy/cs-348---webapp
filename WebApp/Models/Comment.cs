﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Comment
    {
        [Required(ErrorMessage = "Username required")]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} " +
                                         "characters long.", MinimumLength = 2)]
        public string Author { get; set; }

        [Required(ErrorMessage = "No comment text provided!")]
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }

        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;
        public bool IsPublic { get; set; }
        public Guid UniqueId { get; set; }
    }
}
