﻿using System.Collections.Generic;

namespace WebApp.Models
{
    public interface IFileSystem
    {
        bool FileExists(string path);
        string ReadFileText(string path);
        void WriteFileText(string path, string text);
        void DeleteFile(string path);

        void CreateDirectory(string path);
        IEnumerable<string> EnumerateFiles(string directoryPath);
        void AppendFile(string path, byte[] data, int offset, int count);
    }
}
