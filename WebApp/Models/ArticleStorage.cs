﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;

namespace WebApp.Models
{
    public class ArticleStorage
    {
        private const string UploadsFolder = "wwwroot\\Uploads";
        private const string ArticlesFolder = "Article Files\\Articles";
        private const string DraftsFolder = "Article Files\\Drafts";
        private readonly IFileSystem _fileSystem;

        public ArticleStorage(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
            InitStorageFolders();
        }


        private void InitStorageFolders()
        {
            _fileSystem.CreateDirectory(ArticlesFolder);
            _fileSystem.CreateDirectory(DraftsFolder);
            _fileSystem.CreateDirectory(UploadsFolder);
        }


        private static void SetId(Article article)
        {
            if (article.Id == Guid.Empty)
            {
                article.Id = Guid.NewGuid();
            }
        }


        private XDocument LoadArticleXml(string filePath)
        {
            string text = _fileSystem.ReadFileText(filePath);
            var reader = new StringReader(text);
            return XDocument.Load(reader);
        }


        private static IEnumerable<XElement> GetCommentRoot(XDocument doc)
        {
            IEnumerable<XElement> commentRoot = doc.Root?.Elements("Comments");
            return commentRoot;
        }


        private static void IterateComments(IEnumerable<XElement> comments,
            ICollection<Comment> listAllComments)
        {
            foreach (XElement comment in comments)
            {
                var newComment = new Comment
                {
                    Author = comment.Element("Author")?.Value,
                    Body = comment.Element("CommentBody")?.Value,
                    Created = DateTimeOffset.Parse(comment.Element("Created")?.Value),
                    IsPublic = Convert.ToBoolean(comment.Element("IsPublic")?.Value),
                    UniqueId = Guid.Parse(comment.Element("UniqueId")?.Value)
                };
                listAllComments.Add(newComment);
            }
        }


        private static List<Comment> GetAllComments(XDocument doc)
        {
            IEnumerable<XElement> commentRoot = GetCommentRoot(doc);
            var listAllComments = new List<Comment>();
            IEnumerable<XElement> xElements = commentRoot.ToList();
            if (!xElements.Any()) return listAllComments;

            IEnumerable<XElement> comments = xElements.Elements("Comment");
            IterateComments(comments, listAllComments);

            return listAllComments;
        }


        public static Comment FindComment(Guid uniqueId, Article article)
        {
            List<Comment> commentsList = article.Comments;
            foreach (Comment comment in commentsList)
            {
                if (comment.UniqueId.Equals(uniqueId))
                {
                    return comment;
                }
            }
            return null;
        }


        private static void AddComments(Article article, XContainer rootNode)
        {
            var commentsNode = new XElement("Comments");

            foreach (Comment comment in article.Comments)
            {
                var commentNode = new XElement("Comment");
                commentNode.Add(new XElement("Author", comment.Author));
                commentNode.Add(new XElement("Created",
                    comment.Created.ToString("o")));
                commentNode.Add(new XElement("CommentBody", comment.Body));
                commentNode.Add(new XElement("IsPublic", comment.IsPublic));
                commentNode.Add(new XElement("UniqueId", comment.UniqueId));
                commentsNode.Add(commentNode);
            }

            rootNode.Add(commentsNode);
        }


        private static void AppendArticleInfo(Article article, XContainer rootNode)
        {
            rootNode.Add(new XElement("Author", article.Author));
            rootNode.Add(new XElement("Id", article.Id.ToString("N")));
            rootNode.Add(new XElement("Slug", article.Slug));
            rootNode.Add(new XElement("Title", article.Title));
            rootNode.Add(new XElement("Body", article.Body));
            rootNode.Add(new XElement("Created",
                article.PubDate.ToString("o")));
            rootNode.Add(new XElement("LastModified",
                article.LastModified.ToString("o")));
            rootNode.Add(new XElement("IsPublic", article.IsPublic.ToString()));
            rootNode.Add(new XElement("IsDeleted", article.IsDeleted.ToString()));
            rootNode.Add(new XElement("Excerpt", article.Excerpt));
        }

        
        public void SaveArticle(Article article)
        {
            SetId(article);
            string outputFilePath;
            if (article.IsPublic)
            {
                string date = article.PubDate.UtcDateTime.ToString("s")
                    .Replace(":", "-");
                outputFilePath =
                    $"{ArticlesFolder}\\{date}_{article.Id:N}.xml";
            }
            else
            {
                outputFilePath = $"{DraftsFolder}\\{article.Id:N}.xml";
            }

            var doc = new XDocument();
            var rootNode = new XElement("Article");

            AppendArticleInfo(article, rootNode);
            AddComments(article, rootNode);
            doc.Add(rootNode);

            using (var ms = new MemoryStream())
            {
                doc.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(ms))
                {
                    string text = reader.ReadToEnd();
                    _fileSystem.WriteFileText(outputFilePath, text);
                }
            }
        }


        private Article CollectArticleInfo(string expectedFilePath)
        {
            var hasIdChanged = false;
            XDocument doc;
            try
            {
                doc = LoadArticleXml(expectedFilePath);
            }
            catch
            {
                return null;
            }

            var article = new Article();
            if (doc.Root?.Element("Id") != null && !doc.Root.Element("Id").IsEmpty)
            {
                if (Guid.TryParse(doc.Root.Element("Id")?.Value, out Guid newGuid))
                {
                    article.Id = newGuid;
                }
                else
                {
                    _fileSystem.DeleteFile(expectedFilePath);
                    SetId(article);
                    hasIdChanged = true;
                }
            }
            else
            {
                article.Id = Guid.NewGuid();
            }

            if (doc.Root != null)
            {
                article.Author = GetValue(doc.Root.Element("Author"), "");
                article.Slug = GetValue(doc.Root.Element("Slug"), "");
                article.Title = GetValue(doc.Root.Element("Title"), "");
                article.Body = GetValue(doc.Root.Element("Body"), "");
                article.PubDate = GetValue(doc.Root.Element("Created"),
                    default(DateTimeOffset));
                article.LastModified = GetValue(doc.Root.Element("LastModified"),
                    default(DateTimeOffset));
                article.IsPublic = GetValue(doc.Root.Element("IsPublic"), true);
                article.IsDeleted = GetValue(doc.Root.Element("IsDeleted"), false);
                article.Excerpt = GetValue(doc.Root.Element("Excerpt"), "");
            }

            article.Comments = GetAllComments(doc);
            if (hasIdChanged)
            {
                SaveArticle(article);
            }

            return article;
        }


        private static string GetValue(XElement e, string defaultValue)
        {
            if (e != null && !e.IsEmpty)
            {
                return e.Value;
            }

            return defaultValue;
        }


        private static DateTimeOffset GetValue(XElement e,
            DateTimeOffset defaultValue)
        {
            if (e != null && !e.IsEmpty)
            {
                return DateTimeOffset.Parse(e.Value);
            }

            return defaultValue;
        }


        private static bool GetValue(XElement e, bool defaultValue)
        {
            if (e != null && !e.IsEmpty)
            {
                return Convert.ToBoolean(e.Value);
            }

            return defaultValue;
        }


        public Article GetArticle(string id)
        {
            string expectedFilePath = $"{DraftsFolder}\\{id}.xml";
            if (_fileSystem.FileExists(expectedFilePath))
            {
                return CollectArticleInfo(expectedFilePath);
            }

            List<string> files = _fileSystem.EnumerateFiles(
                $"{ArticlesFolder}").ToList();
            foreach (string file in files)
            {
                int start = file.IndexOf("_", StringComparison.Ordinal);
                int end = file.IndexOf(".", StringComparison.Ordinal);
                string element = file.Substring(start + 1, end - start - 1);
                if (element == id)
                {
                    return CollectArticleInfo(file);
                }
            }
            return null;
        }


        private List<Article> IterateArticles(List<string> files)
        {
            var allArticles = new List<Article>();
            foreach (string file in files)
            {
                Article article = CollectArticleInfo(file);
                allArticles.Add(article);
            }
            return allArticles;
        }


        public IEnumerable<Article> GetAllArticles()
        {
            string filePath = $"{ArticlesFolder}";
            List<string> files = _fileSystem.EnumerateFiles(filePath)
                .OrderByDescending(f => f).ToList();
            return IterateArticles(files);
        }


        public IEnumerable<Article> GetAllDrafts()
        {
            string filePath = $"{DraftsFolder}";
            List<string> files = _fileSystem.EnumerateFiles(filePath)
                .OrderByDescending(f => f).ToList();
            return IterateArticles(files);
        }


        public void UpdateArticle(Article article, bool wasPublic)
        {
            if (wasPublic)
            {
                string date = article.PubDate.UtcDateTime.ToString("s")
                    .Replace(":", "-");
                _fileSystem.DeleteFile(
                    $"{ArticlesFolder}\\{date}_{article.Id:N}.xml");
            }
            else
            {
                _fileSystem.DeleteFile(
                    $"{DraftsFolder}\\{article.Id:N}.xml");
            }
            SaveArticle(article);
        }


        public void SaveFiles(IEnumerable<IFormFile> files)
        {
            foreach (IFormFile file in files)
            {
                if (file.Length <= 0) continue;
                
                using (Stream uploadedFileStream = file.OpenReadStream())
                {
                    string name = Path.GetFileName(file.FileName);
                    string filePath = Path.Combine(UploadsFolder, name);

                    if (CheckFileNameExists(name))
                    {
                        _fileSystem.DeleteFile(filePath);
                        continue;
                    }

                    var buffer = new byte[1024];
                    int bytesRead;
                    do
                    {
                        bytesRead = uploadedFileStream.Read(
                            buffer, 0, 1024);
                        if (bytesRead == 0)
                        {
                            break;
                        }

                        _fileSystem.AppendFile(filePath, buffer,
                            0, bytesRead);
                    } while (bytesRead > 0);
                }
            }
        }


        private bool CheckFileNameExists(string filePath)
        {
            return _fileSystem.FileExists(filePath);
        }
    }
}