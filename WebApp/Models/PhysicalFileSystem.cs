﻿using System.Collections.Generic;
using System.IO;

namespace WebApp.Models
{
    public class PhysicalFileSystem : IFileSystem
    {
        public void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
        }

        public void DeleteFile(string path)
        {
            File.Delete(path);
        }

        public bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public string ReadFileText(string path)
        {
            return File.ReadAllText(path);
        }

        public void WriteFileText(string path, string text)
        {
            File.WriteAllText(path, text);
        }

        public IEnumerable<string> EnumerateFiles(string directoryPath)
        {
            return Directory.EnumerateFiles(directoryPath);
        }

        public void AppendFile(string path, byte[] data, int offset, int count)
        {
            using (Stream outStream = File.OpenWrite(path))
            {
                outStream.Seek(0, SeekOrigin.End);
                outStream.Write(data, offset, count);
            }
        }
    }
}