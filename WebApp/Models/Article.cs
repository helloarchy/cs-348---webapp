﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Article
    {
        public Guid Id { get; set; }
        public List<Comment> Comments { get; set; } = new List<Comment>();
        
        // Title 
        [Required(ErrorMessage = "Title required")]
        [DataType(DataType.Text)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} " +
                                         "characters long.", MinimumLength = 2)]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        // Body
        [Required(ErrorMessage = "Article text required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Body")]
        public string Body { get; set; }
        
        // Excerpt
        [DataType(DataType.Text)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} " +
                                          "characters long.", MinimumLength = 2)]
        [Display(Name = "Excerpt")]
        public string Excerpt { get; set; }
        
        // Internally set
        public string Author { get; set; }
        public DateTimeOffset PubDate { get; set; }
        public DateTimeOffset LastModified { get; set; } = DateTimeOffset.Now;
        public string Slug { get; set; }
        public bool IsPublic { get; set; }
        public bool IsDeleted { get; set; }
    }
}
