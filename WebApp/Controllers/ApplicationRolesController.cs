﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Policy = "ManageRoles")]
    public class ApplicationRolesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public ApplicationRolesController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }


        // GET: ApplicationRoles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Roles.ToListAsync());
        }


        // GET: ApplicationRoles/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationRole applicationRole = await _context.Roles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationRole == null)
            {
                return NotFound();
            }

            ApplicationRoleViewModel viewModel =
                await GetRolesViewModelFromRole(applicationRole);

            return View(viewModel);
        }


        // Post: ApplicationRoles/Details
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Details(ApplicationRoleViewModel viewModel)
        {
            if (!ModelState.IsValid) return View(viewModel);

            // Add user to new role and remove from old
            ApplicationUser user =
                await _userManager.FindByNameAsync(viewModel.UserName);

            // If no user, do nothing.
            if (user == null) return View(viewModel);

            IList<string> currentRoles = await _userManager.GetRolesAsync(user);
            await _userManager.AddToRoleAsync(user, viewModel.Role);
            foreach (string currentRole in currentRoles)
            {
                await _userManager.RemoveFromRoleAsync(user, currentRole);
            }

            // Add the claim
            await _userManager.AddClaimAsync(user,
                new Claim(viewModel.Role, "True"));

            // Save changes to database
            await _context.SaveChangesAsync();

            // Create view from role
            ApplicationRole role =
                await _roleManager.FindByNameAsync(viewModel.Role);
            viewModel = await GetRolesViewModelFromRole(role);

            return View(viewModel);
        }


        // Get an Application Roles view model from a given role.
        private async Task<ApplicationRoleViewModel> GetRolesViewModelFromRole(
            ApplicationRole applicationRole)
        {
            var viewModel = new ApplicationRoleViewModel
            {
                ApplicationRole = applicationRole
            };

            /*IList<ApplicationUser> roleUsers = await
                _userManager.GetUsersInRoleAsync(applicationRole.ToString());*/
            IList<ApplicationUser> roleUsers = await
                _userManager.GetUsersForClaimAsync(
                    new Claim(applicationRole.Name, "True"));

            viewModel.ApplicationUsers = roleUsers;

            return viewModel;
        }


        // GET: ApplicationRoles/Create
        public IActionResult Create()
        {
            return View();
        }


        // POST: ApplicationRoles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name, Description")] string name, string description)
        {
            var applicationRole = new ApplicationRole
            {
                Name = name,
                Description = description
            };
            if (!ModelState.IsValid) return View(applicationRole);

            _context.Add(applicationRole);
            await _roleManager.CreateAsync(applicationRole);
            await _roleManager.SetRoleNameAsync(applicationRole, name);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        // GET: ApplicationRoles/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationRole applicationRole = await _context.Roles.FindAsync(id);
            if (applicationRole == null)
            {
                return NotFound();
            }

            return View(applicationRole);
        }


        // POST: ApplicationRoles/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id, Name, Description")] string id, string name,
            string description)
        {
            // Get the role and change the name(s)
            ApplicationRole role = await _roleManager.FindByIdAsync(id);
            string oldName = role.Name;
            if (name != null)
            {
                role.Name = name;
                role.NormalizedName = name.Normalize();
                // Update claim name for all users who have it
                await UpdateClaim(name, oldName);
            }

            // Add description if there is one
            if (!string.IsNullOrEmpty(description))
            {
                role.Description = description;
            }

            _context.Update(role);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }


        // GET: ApplicationRoles/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationRole applicationRole = await _context.Roles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationRole == null)
            {
                return NotFound();
            }

            return View(applicationRole);
        }


        // POST: ApplicationRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            // Get role name and remove role
            ApplicationRole applicationRole = await _context.Roles.FindAsync(id);
            string name = applicationRole.Name;

            // Remove claim for all users who have it, and remove role.
            await RemoveClaim(name);
            _context.Roles.Remove(applicationRole);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        private async Task UpdateClaim(string newName, string oldName)
        {
            // Get all users with the claim and update them
            var oldClaim = new Claim(oldName, "True");
            var newClaim = new Claim(newName, "True");
            IList<ApplicationUser> usersWithClaim =
                await _userManager.GetUsersForClaimAsync(oldClaim);
            foreach (ApplicationUser user in usersWithClaim)
            {
                await _userManager.RemoveClaimAsync(user, oldClaim);
                await _userManager.AddClaimAsync(user, newClaim);
            }
        }


        private async Task RemoveClaim(string name)
        {
            // Remove the claim from all user who have it
            var claim = new Claim(name, "True");
            IList<ApplicationUser> usersInClaim =
                await _userManager.GetUsersForClaimAsync(claim);
            await _userManager.GetUsersForClaimAsync(claim);
            foreach (ApplicationUser user in usersInClaim)
            {
                await _userManager.RemoveClaimAsync(user, claim);
            }
        }
    }
}