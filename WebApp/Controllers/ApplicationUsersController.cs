using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Policy = "ManageUsers")]
    public class ApplicationUsersController : Controller
    {
        private static ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUsersController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        public async Task<IActionResult> Index()
        {
            return View(await _context.Users.ToListAsync());
        }


        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Username, Email, FirstName, LastName, Password, PasswordConfirm")]
            string username, string firstName, string lastName, string email,
            string password, ApplicationUserViewModel applicationUserView)
        {
            if (!ModelState.IsValid) return View(applicationUserView);

            var applicationUser = new ApplicationUser
            {
                UserName = username,
                Email = email,
                FirstName = firstName,
                LastName = lastName
            };

            IdentityResult result = await _userManager.CreateAsync(
                applicationUser, password);
            if (result.Succeeded)
            {
                // Add default role and claim
                await _userManager.AddToRoleAsync(applicationUser, "Customer");
                await _userManager.AddClaimAsync(applicationUser,
                    new Claim("Customer", "True"));
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        // Get: ApplicationUser/Edit
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationUser applicationUser = await _context.Users.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }


        // POST: ApplicationUser/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(
            [Bind("UserName, Email, FirstName, LastName")]
            string id, string userName, string firstName, string lastName)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            user.UserName = userName;
            user.NormalizedUserName = userName.Normalize();
            user.FirstName = firstName;
            user.LastName = lastName;

            _context.Update(user);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }


        // Get: ApplicationUsers/Details
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationUser viewModel = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }


        // Post: ApplicationUsers/Details
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Details(ApplicationUser viewModel)
        {
            if (!ModelState.IsValid) return View(viewModel);
            viewModel = await _userManager.FindByNameAsync(viewModel.UserName);
            return View(viewModel);
        }


        // GET: ApplicationUser/Delete
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationUser applicationUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }


        // POST: ApplicationUser/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = await _context.Users.FindAsync(id);
            _context.Users.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}