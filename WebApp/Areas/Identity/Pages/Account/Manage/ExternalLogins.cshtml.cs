﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Models;

namespace WebApp.Areas.Identity.Pages.Account.Manage
{
    [ValidateAntiForgeryToken]
    public class ExternalLoginsModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly HtmlEncoder _htmlEncoder;

        public ExternalLoginsModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, HtmlEncoder htmlEncoder)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _htmlEncoder = htmlEncoder;
        }

        
        public IList<UserLoginInfo> CurrentLogins { get; set; }

        
        public IList<AuthenticationScheme> OtherLogins { get; set; }

        
        public bool ShowRemoveButton { get; set; }

        
        [TempData] 
        public string StatusMessage { get; set; }

        
        public async Task<IActionResult> OnGetAsync()
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound("Unable to load user with ID " +
                                $"'{_userManager.GetUserId(User)}'.");
            }

            CurrentLogins = await _userManager.GetLoginsAsync(user);
            OtherLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync())
                .Where(auth => CurrentLogins.All(ul => auth.Name != ul.LoginProvider))
                .ToList();
            ShowRemoveButton = user.PasswordHash != null || CurrentLogins.Count > 1;
            return Page();
        }

        
        public async Task<IActionResult> OnPostRemoveLoginAsync(
            string loginProvider, string providerKey)
        {
            string safeLoginProvider = _htmlEncoder.Encode(loginProvider);
            string safeProviderKey = _htmlEncoder.Encode(providerKey);

            ApplicationUser user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(
                    "Unable to load user with ID " +
                    $"'{_userManager.GetUserId(User)}'.");
            }

            IdentityResult result = await _userManager.RemoveLoginAsync(user,
                safeLoginProvider, safeProviderKey);
            if (!result.Succeeded)
            {
                string userId = await _userManager.GetUserIdAsync(user);
                throw new InvalidOperationException(
                    "Unexpected error occurred removing external " +
                    $"login for user with ID '{userId}'.");
            }

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "The external login was removed.";
            return RedirectToPage();
        }


        public async Task<IActionResult> OnPostLinkLoginAsync(string provider)
        {
            string safeProvider = _htmlEncoder.Encode(provider);

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            // Request a redirect to the external login provider to link a
            // login for the current user
            string redirectUrl = Url.Page("./ExternalLogins",
                pageHandler: "LinkLoginCallback");
            AuthenticationProperties properties =
                _signInManager.ConfigureExternalAuthenticationProperties(
                    safeProvider, redirectUrl, _userManager.GetUserId(User));
            return new ChallengeResult(safeProvider, properties);
        }


        public async Task<IActionResult> OnGetLinkLoginCallbackAsync()
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound("Unable to load user with ID " +
                                $"'{_userManager.GetUserId(User)}'.");
            }

            ExternalLoginInfo info = await _signInManager
                .GetExternalLoginInfoAsync(await _userManager.GetUserIdAsync(user));
            if (info == null)
            {
                throw new InvalidOperationException(
                    "Unexpected error occurred loading external " +
                    $"login info for user with ID '{user.Id}'.");
            }

            IdentityResult result = await _userManager.AddLoginAsync(user, info);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException(
                    $"Unexpected error occurred adding external " +
                    $"login for user with ID '{user.Id}'.");
            }

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            StatusMessage = "The external login was added.";
            return RedirectToPage();
        }
    }
}