﻿using Microsoft.AspNetCore.Hosting;
using WebApp.Areas.Identity;

[assembly: HostingStartup(typeof(IdentityHostingStartup))]
namespace WebApp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                /*services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("DefaultConnection")));*/

                /*services.AddDefaultIdentity<ApplicationUser>(config =>
                    {
                        config.SignIn.RequireConfirmedEmail = false;
                    })
                    .AddEntityFrameworkStores<ApplicationDbContext>();*/
            });
        }
    }
}