﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var btnOutlineLight = $(".btn-outline-light");
btnOutlineLight.mouseenter(function () {
    $(this).removeClass("text-light");
    $(this).addClass("bg-secondary text-white");
});
btnOutlineLight.mouseleave(function () {
    $(this).removeClass("bg-secondary text-white");
    $(this).addClass("text-light");
});